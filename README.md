# Hell Triangle

## The challenge

Given a triangle of numbers, find the maximum total from top to bottom.(Since wasn't specified, I decided to use only numbers from 1 to 9)

Example:

6

3 5

9 7 1

4 6 8 4

In this triangle the maximum total is: 6 + 5 + 7 + 8 = 26

An element can only be summed with one of the two nearest elements in the next row.

For example: The element 3 in the 2nd row can only be summed with 9 and 7, but not with 1

## Input

Your code will receive an (multidimensional) array as input.

The triangle from above would be:

Example = [[6],[3,5],[9,7,1],[4,6,8,4]]

### Output

In this triangle the maximum total is: **6 + 5 + 7 + 8 = 26**

### Implementation

The language used was Python 3, because of my familiarity and personal preference, besides is a language easy to learn and work with.

The implementation was made in a very simples way. (Since the project is also very simple)

All methods were created in the `hell_triangle.py` file. It was created three methods:

- `analise_triangle_structure`: To check the structure of the triangle. To check if all levels has x + 1 elements from the level before.
- `get_triangle_sum`: The method to return the sum found in that triangle
- `get_elements`: The main method that will navigate the triangle and get the numbers that will be sum later

## Testing

For the tests I used the **[pytest](https://pytest.org/)** framework. An framework easy to use and to understand it's flow.

All test are in the `tests.py` file.

### Tests

Using the framework I implemented 5 tests:

- Testing the triangle from pdf;
- Testing a triangle with an irregular structure;
- Testing a pre-defined triangle;
- Testing a random triangle;
- Testing a triangle with an irregular structure that should fail.

### How to run

After cloning the repo:

- Open terminal/cmd inside the folder and run `pip install -r requirements.txt` to install de dependencies. (I know that only has pytest ;p)
- Still on terminal/cmd inside the folder, run `pytest -q tests.py`

The output will be traceback for the test 5 (since it must fail) and the last line will say: **1 failed, 4 passed in <execution time\>**.