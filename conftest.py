import pytest
from random import Random


@pytest.fixture()
def example_triangle():
    triangle = [[6], [3, 5], [9, 7, 1], [4, 6, 8, 4]]
    total = 6 + 5 + 7 + 8
    return triangle, total


@pytest.fixture()
def weird_triangle():
    return [[1], [4, 3, 6], [2, 9], [2, 2, 3, 9, 3], [9, 3, 1, 6, 6, 5], [6, 9, 2, 7]]


@pytest.fixture()
def random_triangle():
    size = Random().randint(1, 9)
    base = []
    for i in range(size):
        base.append([Random().randint(1, 9) for _ in range(i+1)])
    return base


@pytest.fixture()
def defined_triangle():
    triangle = [[5], [7, 6], [2, 3, 8], [1, 7, 2, 8], [5, 4, 3, 6, 4], [5, 6, 8, 3, 1, 6]]
    total = 5 + 7 + 3 + 7 + 4 + 8
    return triangle, total
