from hell_triangle import analise_triangle_structure, get_triangle_sum, get_elements


def test_1(example_triangle):
    """
    Testing the triangle from pdf

    :param tuple(list[list[int]], int) example_triangle:  An (multidimensional) array with integers
    """
    triangle, total = example_triangle
    assert analise_triangle_structure(triangle), "This is not a triangle"
    assert total == get_triangle_sum(triangle)


def test_2(weird_triangle):
    """
    Testing a triangle with an irregular structure

    :param  list[list[int]] weird_triangle: An (multidimensional) array with integers
    """
    assert not analise_triangle_structure(weird_triangle), "This has a regular structure"


def test_3(defined_triangle):
    """
    Testing a normal triangle

    :param tuple(list[list[int]], int) defined_triangle:  An (multidimensional) array with integers
    """
    triangle, total = defined_triangle
    assert analise_triangle_structure(triangle), "This is not a triangle"
    assert total == get_triangle_sum(triangle)


def test_4(random_triangle):
    """
    Testing a random triangle

    Since the length of the triangle and his values were created randomly,
    this test will only analyse structure and print triangle, the elements get it and the value
    to be evaluated later.

    :param list[list[int]] random_triangle: An (multidimensional) array with integers
    """
    print(random_triangle)
    assert analise_triangle_structure(random_triangle), "This is not a triangle"
    print(get_elements(random_triangle))
    print(get_triangle_sum(random_triangle))


def test_5(weird_triangle):
    """
    Testing a triangle with an irregular structure that should fail

    :param  list[list[int]] weird_triangle: An (multidimensional) array with integers
    """
    assert analise_triangle_structure(weird_triangle), "This has a irregular structure"
