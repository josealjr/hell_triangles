def analise_triangle_structure(triangle):
    """
    Method to check the structure of the triangle. To check if all levels has x + 1 elements from the level before.
    :param list[list[int]] triangle: An (multidimensional) array with integers
    :return bool: True if all levels has x + 1 elements from the level before, False otherwise
    """
    for index, level in enumerate(triangle):
        if len(level) != index + 1:
            return False
    return True


def get_triangle_sum(triangle):
    """
    Method to do the sum of the elements found

    :param list[list[int]] triangle: An (multidimensional) array with integers
    :return int: The sum of the elements
    """
    return sum(get_elements(triangle))


def get_elements(triangle):
    """
    Method to navigate in the triangle and save each higher number to be sum later
    :param list[list[int]] triangle: An (multidimensional) array with integers
    :return list[int]: A list with all the elements
    """
    elements = [triangle.pop(0)[0]]
    index = 0
    for level in triangle:
        if level[index] >= level[index + 1]:
            elements.append(level[index])
        else:
            elements.append(level[index + 1])
            index += 1

    return elements
